import argparse


# USAGE
# create a file with name 'input.txt'
# key in the first line
# rest will be text
# output will be in file 'input.txt'

def KSA(key):
    # create identity permutation
    permutation = list(range(256))

    # mix the S list so it will represent a permutation depending on the key
    j = 0
    for i in range(256):
        j = (j + permutation[i] + key[i % len(key)]) % 256

        # swap values
        permutation[i], permutation[j] = (permutation[j], permutation[i])
    return permutation


def PRGA(S):
    # generating the stream key depending on the permutation
    i = 0
    j = 0
    while True:
        i = (i + 1) % 256
        j = (j + S[i]) % 256
        S[i], S[j] = (S[j], S[i])
        # use yield to create a generator,
        # so this will work for any text length without need to pass a length as a argument
        yield S[(S[i] + S[j]) % 256]


def encrypt(key, text):
    # change the key and text to bytes
    key_bytes = [ord(x) for x in key]
    text_bytes = [ord(x) for x in text]

    permutation = KSA(key_bytes)

    # xor the streaming key with the text
    encrypted_bytes = [a ^ b for (a, b) in zip(text_bytes, PRGA(permutation))]

    # Just some python magic so it will look nice as a hex value
    encrypted_hex = ''.join([chr(x) for x in encrypted_bytes])
    return encrypted_hex


parser = argparse.ArgumentParser()
parser.add_argument('key', type=str)

args = parser.parse_args()
key = args.key

# reading and writing to the file
with open('input.txt') as f:
    lines = f.readlines()
    text = ''.join(lines).replace('\n', '')

    with open('output.txt', 'w+') as out:
        out.write(encrypt(key, text))
